# Get CO<sup>2</sup>-emissions

This endpoint returns all CO<sup>2</sup>-emissions of public buildings in San Francisco. 

**URL** : `https://emission-challenge.eu-gb.mybluemix.net/IBMChallengeEmissions/emissions`

**Method** : `GET`

**Auth required** : NO

**Permissions required** : None

**Query Parameter** : `[int start]`

**Query Parameter** : `[int(1..) size] ` default: ` 10 `

**Query Parameter** : ` [String source] `

**Query Parameter** : ` [boolean download] ` default: ` false `

## Success Responses

**Code** : `200 OK`

**Content** : It transforms the CSV dataset and returns `JSON`. Furthermore, the `start` and `size` query-parameters serve as a simple pagination-method. The `download` query-parameter chooses between loading a local version of the exported CSV file (`false`) and retrieving the file from its respective source at `https://data.sfgov.org/Energy-and-Environment/San-Francisco-Municipal-Greenhouse-Gas-Inventory/pxac-sadh` (`true`). The `source` query-parameter filters case-insensitive with a simple string-comparison:

```json
[
    {
        "department": "Airport, San Francisco International (SFO)",
        "source_type": "B20",
        "emissions": 336.66
    },
    {
        "department": "Airport, San Francisco International (SFO)",
        "source_type": "CNG",
        "emissions": 410.96
    },
    {
        "department": "Airport, San Francisco International (SFO)",
        "source_type": "Diesel",
        "emissions": 47.41
    }
]
```