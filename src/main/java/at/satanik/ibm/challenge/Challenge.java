package at.satanik.ibm.challenge;

import at.satanik.ibm.challenge.emissions.EmissionsData;
import at.satanik.ibm.challenge.misc.CSVUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Context;
import java.io.InputStream;
import java.util.List;

@Path("/emissions")
@Produces("application/json")
public class Challenge {

    @Context
    ServletContext servletContext;

    @GET
    public String get(@QueryParam("source") String source_type,
                      @QueryParam("start") int start,
                      @QueryParam("size") int size,
                      @QueryParam("download") boolean download) {
        try {
            InputStream stream;
            if (download) {
                Client client = ClientBuilder.newClient();
                String url    = "https://data.sfgov.org/api/views/pxac-sadh/rows.csv?accessType=DOWNLOAD";
                stream = client.target(url).request().get(InputStream.class);
            } else {
                stream = servletContext.getResourceAsStream("/WEB-INF/San_Francisco_Municipal_Greenhouse_Gas_Inventory.csv");
            }

            List<List<String>> store = CSVUtils.get(stream);

            Gson g = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            return g.toJson(
                    store.stream()
                            .map(EmissionsData::constructFromStrings)
                            .filter(e -> e.getEmissions() > 0 && (source_type == null || e.getSource_type().toLowerCase().equals(source_type.toLowerCase())))
                            .skip(start)
                            .limit(size > 0 ? size : 10)
                            .toArray());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "{ \"key\": \"Hello World\" }";
    }
}
