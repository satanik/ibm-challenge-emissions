package at.satanik.ibm.challenge.emissions;

import com.google.gson.annotations.Expose;

import java.util.List;

public class EmissionsData {
    @Expose private String department;
    @Expose private String source_type;
    private int    fiscal_year;
    private String quarter;
    @Expose private float  emissions;
    private float  consumptions;
    private String consumption_units;

    public static EmissionsData constructFromStrings(List<String> values) {

        EmissionsData d = new EmissionsData();

        d.department = values.get(0);
        d.source_type = values.get(1);
        d.fiscal_year = Integer.parseInt(values.get(2));
        d.quarter = values.get(3);
        d.emissions = values.get(4).equals("N/A") ? 0.0f :Float.parseFloat(values.get(4));
        d.consumptions = values.get(5).equals("N/A") ? 0.0f : Float.parseFloat(values.get(5));
        d.consumption_units = values.get(6);

        return d;
    }

    public String getDepartment() {
        return department;
    }

    public String getSource_type() {
        return source_type;
    }

    public int getFiscal_year() {
        return fiscal_year;
    }

    public String getQuarter() {
        return quarter;
    }

    public float getEmissions() {
        return emissions;
    }

    public float getConsumptions() {
        return consumptions;
    }

    public String getConsumption_units() {
        return consumption_units;
    }
}