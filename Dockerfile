FROM websphere-liberty:microProfile
COPY server.xml /config/
ADD target/IBMChallengeEmissions.war /opt/ibm/wlp/usr/servers/defaultServer/dropins/
ENV LICENSE accept
EXPOSE 9080

## Running the container locally
# mvn clean install
# docker build -t ibmchallengeemissions:latest .
# docker run -d --name myjavacontainer ibmchallengeemissions
# docker run -p 9080:9080 --name myjavacontainer ibmchallengeemissions
# Visit http://localhost:9080/IBMChallengeEmissions/

## Push container to IBM Cloud
# docker tag ibmchallengeemissions:latest registry.ng.bluemix.net/<my_namespace>/ibmchallengeemissions:latest
# docker push registry.ng.bluemix.net/<my_namespace>/ibmchallengeemissions:latest
# ibmcloud ic images # Verify new image
